﻿namespace RomanNumerals
{
    public class BusinessLogic
    {
        private int _numberToConvert;

        public string ConvertNumberToRomanNumeral(int numberToConvert)
        {
            _numberToConvert = numberToConvert;
            var romanNumeral = string.Empty;
            if (_numberToConvert < 1)
            {
                return string.Empty;
            }
            if (_numberToConvert == 9)
            {
                return "IX";
            }
            switch (_numberToConvert)
            {
                case 1:
                    return "I";
                case 2:
                    return "II";
                case 3:
                    return "III";
                case 4:
                    return "IV";
            }
            if (_numberToConvert > 4 && _numberToConvert < 9)
            {
                romanNumeral += "V";
                _numberToConvert -= 5;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }

            if (_numberToConvert > 999)
            {
                romanNumeral += "M";
                _numberToConvert -= 1000;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 899)
            {
                romanNumeral += "CM";
                _numberToConvert -= 900;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 499)
            {
                romanNumeral += "D";
                _numberToConvert -= 500;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 399)
            {
                romanNumeral += "CD";
                _numberToConvert -= 400;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 99)
            {
                romanNumeral += "C";
                _numberToConvert -= 100;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 89)
            {
                romanNumeral += "XC";
                _numberToConvert -= 90;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 49)
            {
                romanNumeral += "L";
                _numberToConvert -= 50;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            if (_numberToConvert > 39)
            {
                romanNumeral += "XL";
                _numberToConvert -= 40;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }
            
            
            if (_numberToConvert > 9)
            {
                romanNumeral += "X";
                _numberToConvert -= 10;
                romanNumeral += ConvertNumberToRomanNumeral(_numberToConvert);
            }

            return romanNumeral;
        }
    }
}